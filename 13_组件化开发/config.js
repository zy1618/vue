const path = require('path')
const webpack = require('webpack')
module.exports = {
  entry: './',
  output: {
    path: '',
    filename: ''
  },
  modeule: {
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: ["style-loader", "css-loader"],
        },
        {
          test: /\.less$/i,
          loader: "less-loader", // 将 Less 文件编译为 CSS 文件
        },
      ],
    },
  },
  resolve: {
    extendsion: ['.vue']
  },
  plugins: [
    new webpack.BannerPlugin('  ')
  ]
}