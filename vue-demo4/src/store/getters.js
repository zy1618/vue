export default {
  mulCounter(state) {
    return state.counter * state.counter
  },
  more15Student(state) {
    return state.student.filter(s => s.age >= 15)
  },
  more15StudentLength(state, getters) {
    return getters.more15Student.length
  },
  moreAgeStudent(state) {
    return age => {
      return state.student.filter(s => s.age > age)
    }
  }
}