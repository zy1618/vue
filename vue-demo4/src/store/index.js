import Vue from 'vue'
import Vuex from 'vuex'
import mutations from "@/store/mutations";
import getters from "@/store/getters";
import actions from "@/store/actions";
import moduleA from "@/store/modules/moduleA";
Vue.use(Vuex)

const state = {
    counter: 100,
    student: [
      {
        id: '101',
        name: 'zs',
        age: 18
      },
      {
        id: '102',
        name: 'zss',
        age: 12
      }
    ],
    info: {
      name: 'haha',
      age: 16
    }
}
const store = new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
  modules: {
    moduleA
  }
})
export default store