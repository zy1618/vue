import Vue from 'vue'
import Vuex from 'vuex'
import * as types from './mutation-types.js'
Vue.use(Vuex)
const moduleA = {
  state: {
    name: 'jim1'
  },
  mutations: {
    updateModuleName(state) {
      state.name = 'james'
    },
    synupdateModuleName(state) {
      state.name = 'curry'
    }
  },
  getters: {
    fullname1(state) {
      return state.name + '111'
    },
    nameandcounter(state,getter,rootState) {
      return state.name + rootState.counter
    }
  },
  actions: {
    synupdateModuleName(context) {
      setTimeout(() => {
        console.log(context)
        context.commit('synupdateModuleName')
      },1000)
    }
  },
  modules: {}
}
const store = new Vuex.Store({
  state: {
    counter: 100,
    student: [
      {
        id: '101',
        name: 'zs',
        age: 18
      },
      {
        id: '102',
        name: 'zss',
        age: 12
      }
    ],
    info: {
      name: 'haha',
      age: 16
    }
  },
  mutations: {
    [types.INCREATMENT](state) {
      state.counter++
    },
    decreatment(state) {
      state.counter--
    },
    addCounter(state, payload) {
      /*state.counter += count*/
      state.counter += payload.count
    },
    addStudent(state, stu) {
      state.student.push(stu)
    },
    updateInfo(state) {
      //未做到响应式修改数据
      /* state.info['address'] = 'bj'*/
      //可做到响应式修改信息
      Vue.set(state.info, 'address', 'sh')
      //未做到响应式修改信息
      /*delete state.info.address*/
     /* Vue.delete(state.info,'address')*/
    }
  },
  actions: {
    aupdateInfo(context,payload) {
      return new Promise((resolve) => {
        setTimeout(() => {
          context.commit('updateInfo');
          console.log(payload);
          resolve('你成功了')
        },1000)
      })
    }
  },
  getters: {
    mulCounter(state) {
      return state.counter * state.counter
    },
    more15Student(state) {
      return state.student.filter(s => s.age >= 15)
    },
    more15StudentLength(state, getters) {
      return getters.more15Student.length
    },
    moreAgeStudent(state) {
      return age => {
        return state.student.filter(s => s.age > age)
      }
    }
  },
  modules: {
    moduleA
  }
})
export default store