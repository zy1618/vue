export default {
  state: {
    name: 'jim1'
  },
  mutations: {
    updateModuleName(state) {
      state.name = 'james'
    },
    synupdateModuleName(state) {
      state.name = 'curry'
    }
  },
  getters: {
    fullname1(state) {
      return state.name + '111'
    },
    nameandcounter(state, getter, rootState) {
      return state.name + rootState.counter
    }
  },
  actions: {
    synupdateModuleName(context) {
      setTimeout(() => {
        console.log(context)
        context.commit('synupdateModuleName')
      },1000)
    }
  },
}