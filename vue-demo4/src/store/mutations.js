import * as types from "@/store/mutation-types";
import Vue from "vue";

export default {
  [types.INCREATMENT](state) {
    state.counter++
  },
  decreatment(state) {
    state.counter--
  },
  addCounter(state, payload) {
    /*state.counter += count*/
    state.counter += payload.count
  },
  addStudent(state, stu) {
    state.student.push(stu)
  },
  updateInfo(state) {
    //未做到响应式修改数据
    /* state.info['address'] = 'bj'*/
    //可做到响应式修改信息
    Vue.set(state.info, 'address', 'sh')
    //未做到响应式修改信息
    /*delete state.info.address*/
    /* Vue.delete(state.info,'address')*/
  }
}